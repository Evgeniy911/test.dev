<?php

namespace app\models\news_category;

use app\models\news\News;
use Yii;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property News[] $news
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['catagory_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return NewsCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsCategoryQuery(get_called_class());
    }
}
