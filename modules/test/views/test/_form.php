<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \app\models\news_category\NewsCategory;

/* @var $this yii\web\View */
/* @var $model app\models\news\News */
/* @var $form yii\widgets\ActiveForm */

$category = NewsCategory::find()->all();
// формируем массив, с ключем равным полю 'id' и значением равным полю 'name'
$items = \yii\helpers\ArrayHelper::map($category, 'id', 'name');
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'category_id')->dropDownList($items); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
