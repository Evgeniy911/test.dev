<?php

use yii\db\Migration;

class m170412_193503_initial extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('news_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'category_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_news_category_id', 'news', 'category_id', 'news_category', 'id', 'CASCADE', 'CASCADE');

        for($i = 0; $i < 10; $i++){
            $model = new \app\models\news_category\NewsCategory();
            $model->name = 'Category_' . $i;
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_news_category_id', 'news');

        $this->dropTable('news_category');
        $this->dropTable('news');
    }
}
